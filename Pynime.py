import requests
from bs4 import BeautifulSoup

# Functions
def searchAnime(search):
    try:
        urls = f"https://gogoanime.ai//search.html?keyword={search}"
        result = requests.get(urls).text
        soup = BeautifulSoup(result, 'lxml')
        animes = soup.find('ul', {'class': 'items'}).find_all('li')
        if str(animes) == "[]": print('Wrong input') 
        else:
            for anime in animes:
                anime_title = anime.a['title']
                anime_category = anime.a['href'].split('/')[2]
                print(anime_category + ' | ' + anime_title)
            
    except requests.exceptions.ConnectionError:
        return ('Something went wrong, please try again later.')

#Testing
searchForAnime = input("Tell me name of anime: ")
searchAnime(searchForAnime)